import './App.css'
import React, {Component} from 'react'
import {FormControl, InputGroup, FormGroup, Glyphicon} from 'react-bootstrap'
import Profile from './Profile'
import Gallery from './Gallery'
import SpotifyRoutes from './constants/SpotifyRoutes'


class App extends Component{
    constructor(props){
        super(props)
        this.state = {
            searchValue: '',
            artist: null,
            tracks: []
        }
    }

    handleChange = (event) =>{
        this.setState({searchValue: event.target.value})
    }

    handleENTERPress = (event) => {
        if (event.key === 'Enter') {
            this.search();
        }
    }

    search(){
        let FETCH_URL = `${SpotifyRoutes.SPOTIFY_SEARCH_URL}?q=${this.state.searchValue}&type=artist&limit=1`
        fetch(FETCH_URL, {
            method: 'GET',
            headers: {
                'Authorization': SpotifyRoutes.SPOTIFY_TOKEN
            }
        }).then(response => response.json())
        .then( json => {
            const artist = json.artists.items[0]
            this.setState({artist: json.artists.items[0]});
            FETCH_URL = `${SpotifyRoutes.SPOTIFY_ARTIST}/${artist.id}/top-tracks?country=US&`
            fetch(FETCH_URL, {
                method: 'GET',
                headers: {
                    'Authorization': SpotifyRoutes.SPOTIFY_TOKEN
                }
            }).then(response => response.json())
            .then( json => {
                const {tracks} = json
                this.setState({tracks});
            });
        });
    }

    render(){
        if (this.state.artist !== null){
            console.log("ARTISTA ENCONTRADO ", this.state.artist)
        }
        return(
            <div className="App">
                <div className="App-title">MusicMaster</div>
                <FormGroup>
                    <InputGroup>
                        <FormControl
                            type="text"
                            placeholder="Search an Artist"
                            value={this.state.searchValue}
                            onChange={this.handleChange}
                            onKeyPress={this.handleENTERPress}
                        />
                        <InputGroup.Addon onClick={() => this.search()}>
                            <Glyphicon glyph="search"/>
                        </InputGroup.Addon>
                    </InputGroup>
                </FormGroup>
                {
                  this.state.artist !== null
                    ? <div>
                        <Profile
                            artist={this.state.artist}/>
                        <Gallery
                            tracks={this.state.tracks}/>
                      </div>
                    : <div></div>
                }
            </div>
        );
    }
}

export default App;
